<?php
	if (!defined('DOKU_COOKIE')) define('DOKU_COOKIE', 'DW'.md5('/projects/zmc/'));
	require_once("../inc/init.php");
	require_once("../inc/auth.php");
	
	$log = auth_login("", "", false, true);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>ZMC - Script Layout</title>
		<meta name="author" content="Isral G. Crisanto (Falleco)">
		<meta name="copyright" content="� 2009, Israel G. Crisanto">
		<link rel="stylesheet" type="text/css" href="main.css" />
		<script type="text/javascript" src="lib/jquery.min.js"></script>    
		<script type="text/javascript" src="lib/jquery-ui.min.js"></script>    
		<script type="text/javascript" src="lib/jquery.fieldselection.js"></script>		
		<script type="text/javascript" src="lib/zmc.js"></script>			
	</head> 

	<body>

		<div id="container">
			<div id="links">
				<?php
					echo  $log ? "Ol�, <span>".ucwords($_SERVER['REMOTE_USER'])."</span>" : '<a href="http://romhacking.trd.br/projects/zmc/doku.php?do=login" target="_SELF">Entrar</a>';
				?><br />
				<a href="http://www.romhacking.trd.br/projects/zmc/doku.php?id=tabelas_de_equivalencia" taget="_BLANK">Refer�ncia</a><br />
			</div>
			<img src="./imgs/zmc_logo.png" alt="Editor de Scripts" />
			<?php
				if(!$log) {
					echo '
						<div class="warn">
							<p>Aten��o! Voc� n�o est� logado no sistema da <a href="http://romhacking.trd.br/projects/zmc/doku.php">WIKI</a> do projeto, por isso n�o conseguir� salvar as altera��es feitas nos scripts.</p>
							<p>Caso queira salvar o seu trabalho, <a href="http://romhacking.trd.br/projects/zmc/doku.php?do=login">entre com seu usu�rio</a> na wiki e retorne a esta p�gina.</p>
						</div>
					';
				}
			?>
			
			<!--XXXXX Parte que exibe e carrega o script XXXXX-->
			<fieldset id="scriptView">
				<legend>Dados do Script</legend>
				<!--<form id="formLoad" method="POST" action="god.php" >-->
					<select id="scriptName">
						<?php
							$SCRIPT_PATH = "scripts/";
							//Open images directory
							$dir = scandir($SCRIPT_PATH);

							//List files in images directory
							foreach ($dir as $file)
								if(($file != ".") && ($file != ".."))
									echo "<option value=\"$file\" >".substr($file, 0, strrpos($file,'.'))."</option>";
						?> 
					</select>
					<button id="loadFile">Carregar Script</button>
					<div id="textData"><textarea id="txtData" cols="60" rows="18"></textarea></div>
					<div id="msg" ></div><button disabled><<</button><button disabled>>></button><button id="save" <?php echo ($log ? "": "disabled" )?>>Salvar</button>
				<!--</form>-->
			</fieldset>
			
			<!--XXXXX Parte que exibe o preview XXXXX-->
			<fieldset class="scriptPreview">
				<legend>Preview</legend>
				<center>
				<div id="prevText">	</div>
				<br />
				</center>	
				<textarea id="preview" cols="50" rows="2"></textarea>				
			</fieldset>  
			
			<!--XXXXX Mostra o texto sem ser processado, s� para debug XXXXX-->
			<br />
			<fieldset class="scriptPreview">
				<legend>Op��es</legend>
				Nome do her�i: <input value="Link" id="heroName" maxlength="6" /><br />
				Tipo de Di�logo:
					<input type="radio" name="Schema" id="Schema0" checked />Comum
					<input type="radio" name="Schema" id="Schema1" />T�cnicas
					<input type="radio" name="Schema" id="Schema2" />Fig. Descri��es
					<input type="radio" name="Schema" id="Schema3" />Fig. Nomes
			</fieldset>  
		</div>
	</body>
</html>