/**
* Define o limite m�ximo, em pixel, das linhas de texto
*/
var LINE_WIDTH ;

/**
* Espa�amento entre as linhas
*/
var LINE_SPACE ;

/**
* Quantidade de linhas
*/
var LINE_NUMBER;

/**
* Altura, em pixel, da linha
*/
var LINE_HEIGHT;

/**
* Posi��o Y do inicio dos textos
*/
var POS_TOP ;

/**
* Posi��o X do inicio dos textos
*/
var POS_LEFT ;

/**
* Cor de fonte padr�o a ser utilizada nos di�logos, nos
* di�logos comuns, � a cor branca
*/
var DEFAULT_COLOR;

/**
* Array que armazenar� as larguras das letras
*/
var vwf;

/** 
* C�digo que ser� executado quando a p�gina for carregada
*/      
$(document).ready(function() {
	// Inicializa algumas coisas
	initZMC();
	
	// Di�logo comum
	$("#Schema0").mouseup( function() {
		$("#prevText").css("background-image", "url('imgs/bg_common.png')");
		
		DEFAULT_COLOR = "branco";
		LINE_WIDTH = 208;
		LINE_SPACE = 0;
		LINE_NUMBER = 2;
		LINE_HEIGHT = 16;
		POS_TOP = 119;
		POS_LEFT = 16;
		
		// Cria as linhas de acordo com as configura��es do esquema
		createLines();
		
		// For�a a atualiza��o do preview
		$('textarea').trigger("keyup");
	});	
	
	// t�cnicas com espadas
	$("#Schema1").mouseup( function() {
		$("#prevText").css("background-image", "url('imgs/bg_skills.png')");
		DEFAULT_COLOR = "preto";
		LINE_WIDTH = 147;
		LINE_SPACE = 0;
		LINE_NUMBER = 4;
		LINE_HEIGHT = 16;		
		POS_TOP = 77;
		POS_LEFT = 64;
		createLines();
		$('textarea').trigger("keyup");
	});	
	
	// Figurino descri��es
	$("#Schema2").mouseup( function() {
		$("#prevText").css("background-image", "url('imgs/bg_figurino.png')");
		DEFAULT_COLOR = "preto";
		LINE_WIDTH = 222;
		LINE_SPACE = 0;
		LINE_NUMBER = 2;
		LINE_HEIGHT = 16;		
		POS_TOP = 120;
		POS_LEFT = 8;
		createLines();
		$('textarea').trigger("keyup");
	});	
	
	// Figurino nomes
	$("#Schema3").mouseup( function() {
		$("#prevText").css("background-image", "url('imgs/bg_figurino.png')");
		DEFAULT_COLOR = "branco";
		LINE_WIDTH = 136;
		LINE_SPACE = 0;
		LINE_NUMBER = 1;
		LINE_HEIGHT = 16;		
		POS_TOP = 64;
		POS_LEFT = 88;
		createLines();
		$('textarea').trigger("keyup");
	});	
	
	// Simula um clique de mouse, para inicializar os valores padr�es das
	// vari�veis globais, que ser�o usadas para o esquema padr�o
	$("#Schema0").trigger("mouseup");
	
	// Faz a leitura da tabela de larguras vari�veis
	$.getJSON("lib/vwf.js", function(json){ vwf = json.vwf; });
	
	// Cuida do carregamento do texto
	$("#loadFile").click( function() {
		if ($("#scriptName").val()) { 
			// L� os dados do arquivo de texto selecionado direto no textarea
			$.ajax({
			   async: false,
			   processData: false,
			   type: "GET",
			   url: "god.php",
			   data: "type=get&script=" + $("#scriptName").val(),
			   success: function(txt){
				 $('textarea').val(txt);
			   }
			});
			
			$('#preview').val("");
		}
	});

	// O que acontece quando clicamos em 'Salvar'
	$("#save").click( function() {
		$.ajax({
		   async: false,
		   type: "POST",
		   dataType: "text",
		   processData: false,
		   url: "god.php",
		   data: "type=save&script=" + $("#scriptName").val()+"&data=" + encodeURI($('#txtData').val()),
		   success: function(txt){
				// Mensagem que aparece quando tivermos terminado de carregar algo
				if(txt == "SALVO") {
					$("#msg")
						.text("Altera��es salvas!")
						.css("background-color", "Green")
						.css("color", "white")
						.slideUp(500)
				} else {
					$("#msg")
						.text("Erro ao salvar informa��es!!!")
						.css("background-color", "Red")
						.css("color", "white")
						.slideUp(500)
				}
			}
		});
	});

	// Define os eventos utilizados para atualizaar a tela,
	// ou seja finalmente d� inicio ao script
	setEvents();	
});

/**
* Limpa o conte�do do container principal do preview e o preenche novamente
* com divs que representar�o as 'linhas' do script, tudo de acordo com as configura��es
* das vari�ves globais, setadas com valores diferentes para cada esquema
*/
function createLines() {
	// Elimina o conte�do anterior
	$("#prevText").html("");
	
	// Cria as novas linhas
	for(var l = 1; l <= LINE_NUMBER; l++) {
		$("#prevText").append(
			$("<div></div>")
				.attr("id", "line" + l)
				
				.height(LINE_HEIGHT)
				.width(LINE_WIDTH)
				
				.css("position", "absolute")
				
				.css("left", POS_LEFT+"px")
				.css("top",  (POS_TOP + (l - 1) * LINE_HEIGHT) + (l != 1 ? LINE_SPACE : 0) +"px")
				
				.css("padding", "0")
				.css("margin",  "0")
		);
	}
}

/**
* Inicializa��es basicas para que o sistema funcione
*/
function initZMC() {
	// Pre-load das imagens
	$.preloadImages("imgs/alfabeto_branco.png", "imgs/alfabeto_azul.png", "imgs/alfabeto_vermelho.png",
		"imgs/alfabeto_verde.png", "imgs/cornetinha.png", "imgs/icones.png", "imgs/bg_common.png",
		"imgs/bg_skills.png", "imgs/bg_figurino.png");
	
	// Esconde a div de mensagens
	$("#msg").hide();
	
	// Algumas configura��es para ajax
	$.ajaxSetup({ async: false }); 	// Melhor esperar at� que o pedido seja feito para poder continuar
	
	// Mensagem que aparece quando estivermos carregando algo
	$("#msg").ajaxStart(function(){
		$(this).text("CARREGANDO!");
		$(this).css("background-color", "Yellow");
		$(this).css("color", "black");
		$(this).slideDown(500);
	});
	
	// Mensagem que aparece quando tivermos terminado de carregar algo
	$("#msg").ajaxComplete(function(request, settings){
		$(this).text("Pronto!");
		$(this).css("background-color", "Green");
		$(this).css("color", "white");
		$(this).slideUp(500);
	});
	
	// Caixa de ajuda
	$("#helpLink").click(function () { 
		$.modaldialog.prompt(
			'N�o h� muito o que descrever sobre este utilit�rio, quem sabe um dia...',
			{ title: 'Ajuda para utilizar o editor'}
		);
	});
	
	// Deixa, por enquanto, o bot�o de salvar desativado
	//$("#save").attr("disabled", "disabled");
}

/**
* Utilizamos eventos para controlar a forma de atualiza��o
* do preview.
*
* Os eventos mais comuns ser�o quando o usu�rio digitar algo
* no textarea, ou quando ele clicar, mudando a posi��o do carro
* de leitura.
*
* Esta fun��o tamb�m filtra os eventos, por exemplo, o clique do mouse,
* quando gerar scroll n�o ativar� a atualiza��o.
*
* Eventos posteriores ser�o adicionados
* @param none
* @return none
*/
function setEvents() {
	$('textarea').keyup(update).mouseup(update);
}

/**
* Fun��o respons�vel pela atualiza��o da janela de preview de acordo
* com os eventos correspondentes.
* @param none
* @return none
*/
function update() {
	var range = $(this).getSelection();
	
	// Simplesmente pega as linhas que devem ser desenhadas na tela
	lines = getBlock(this.value, range.start);
	
	// Uma janela que exibe o texto puro
	$('#preview').val(lines);
	
	// Desenha as linhas na tela
	drawLines(lines.split('\n'));
}

/**
* Aqui utilizamos divs e algumas caracter�sticas de backgrounds para simular a escrita
* em tempo real dos textos de forma muito parecida com o que ocorre dentro do jogo.
*
* Os par�metros utilizados, como quantidade de linhas, espa�o entre as linhas e largura
* das mesmas ser�o lidos diretamente de vari�veis globais (por enquanto), sendo passado
* como argumento somente um array com as linhas de textos
*
* @param line � um array que cont�m duas strings que ser�o impressas na tela de preview
* @return none
*/
function drawLines(line) {
	// Cor setada no momento
	var cor = DEFAULT_COLOR;
	
	// Processa todas as linhas, que s�o passadas via array no argumento
	for(var id = 0; id < LINE_NUMBER; id++) {
		// Limpa o conte�do de todas as linhas
		$("#line" + (id + 1)).text("");
		// Tamanho X da linha
		var px = 0;
		
		// S� Processa se houver algo na linha
		if(line[id]) {
			// Substitui a vari�vel do nome
			line[id] = line[id].replace('[VAR:00]', "[VERDE]" + $('#heroName').val() + "[BRANCO]");
			// Remove algumas tags que poder�o ser descartadas
			//line[id] = line[id].replace(/\[[a-zA-Z0-9]+[:]+[a-zA-Z0-9]+\]/g, '');
			for(var x = 0; x < line[id].length; x++) {
				//  Processa algumas tags
				if(line[id][x] == '[') {
					var  tag = new String(line[id].substr(x, 11).match(/\[[a-zA-Z0-9:]+\]/g));
					if(tag == "[BRANCO]") {
						cor = DEFAULT_COLOR;
					}
					else if (tag == "[VERMELHO]") {
						cor = "vermelho"
					}
					else if (tag == "[VERDE]") {
						cor = "verde";
					}
					else if (tag == "[AZUL]") {
						cor = "azul";
					}
					else if (tag.search(/\[ICON:([0-9a-fA-F]+)\]/) != -1) {
						// Separa o valor hexadecimal do bot�o
						var ts  = parseInt(tag.match(/\[ICON:([0-9a-fA-F]+)\]/)[1], 16);
						
						var tpx = 0;
						if(ts == 0xb) tpx = 72;
						else tpx = 80;

						$("#line" + (id + 1)).append(
							$("<div></div>")
								.attr("id", "l" + id + "c" + x)
								.height(16)
								.width(8)
								.css("position", "absolute")
								.css("left", px+"px")
								.css("padding", "0")
								.css("margin", "0")
								.css("background-image", 'url("./imgs/icones.png")')
								.css("background-position", '-' + tpx + 'px -16px')
								.css("background-repeat", "no-repeat")
						);
						px += 8;
					}
					else if (tag.search(/\[OPT:([0-9a-zA-Z]+)\]/) != -1) {
						var ts  = tag.match(/\[OPT:([0-9a-zA-Z]+)\]/)[1];

						if(ts != "END") {
							$("#line" + (id + 1)).append(
								$("<div></div>")
									.attr("id", "l" + id + "c" + x)
									.height(16)
									.width(8)
									.css("position", "absolute")
									.css("left", px+"px")
									.css("padding", "0")
									.css("margin", "0")
									.css("background-image", 'url("./imgs/icones.png")')
									.css("background-position", '-96px -16px')
									.css("background-repeat", "no-repeat")
							);
							px += 8;
						}
					}
					else if (tag.search(/\[SETA:([a-zA-Z]+)\]/) != -1) {
						var ts  = tag.match(/\[SETA:([a-zA-Z]+)\]/)[1];
						var tpx = 0;
						if(ts == "CIMA")	tpx = 48;
						else if(ts == "BAIX")	tpx = 56;
						else if(ts == "DIRE")	tpx = 64;
						else if(ts == "ESQU")	tpx = 72;
						//alert(ts);
						
						$("#line" + (id + 1)).append(
							$("<div></div>")
								.attr("id", "l" + id + "c" + x)
								.height(16)
								.width(8)
								.css("position", "absolute")
								.css("left", px+"px")
								.css("padding", "0")
								.css("margin", "0")
								.css("background-image", 'url("./imgs/icones.png")')
								.css("background-position", '-'+tpx+'px -16px')
								.css("background-repeat", "no-repeat")
						);
						px += 8;
					}
					else if (tag.search(/\[BOTAO:([0-9a-fA-F]+)\]/) != -1) {
						// Separa o valor hexadecimal do bot�o
						var ts  = parseInt(tag.match(/\[BOTAO:([0-9a-fA-F]+)\]/)[1], 16);
						
						var tpx = (ts % 8) * 16;
						var tpy = ((ts - (ts % 8)) / 8) * 16;
						$("#line" + (id + 1)).append(
							$("<div></div>")
								.attr("id", "l" + id + "c" + x)
								.height(16)
								.width(16)
								.css("position", "absolute")
								.css("left", px+"px")
								.css("padding", "0")
								.css("margin", "0")
								.css("background-image", 'url("./imgs/icones.png")')
								.css("background-position", '-' + tpx + 'px -' + tpy + 'px')
								.css("background-repeat", "no-repeat")
						);
						px += 16;
					}
					else if (tag.search(/\[[SFX:]+/) != -1) {
						$("#line" + (id + 1)).append(
							$("<img />")
								.attr("src", "./imgs/cornetinha.png")
								.css("position", "absolute")
								.css("left", (px - 7)+"px")
								.css("top", id == 0 ? "-17px" : "17px" )
						);
					}
					else {
						//alert("TAG desconhecida: " + tag);
					}
					
					x += tag.length -1;
				// Agora lida com o texto
				} else {
					var code = line[id].charCodeAt(x);
					var bpx = ((code - 0x20) % 16) * 8;
					var bpy = (((code - 0x20) - ((code - 0x20) % 16)) / 16) * 16;
					
					$("#line" + (id + 1)).append(
						$("<div></div>")
							.attr("id", "l" + id + "c" + x)
							.height(16)
							.width( vwf[code - 0x20] )
							.css("position", "absolute")
							.css("left", px+"px")
							.css("padding", "0")
							.css("margin", "0")
							.css("background-image", 'url("./imgs/alfabeto_'+cor+'.png")')
							.css("background-position", '-' + bpx + 'px -' + bpy + 'px')
							.css("background-repeat", "no-repeat")

					);
					px += vwf[code - 0x20];
					
					// Alerta visual de que o texto ultrapassou os limites
					if(px >= (LINE_WIDTH - 1)) {
						$("#preview").effect("highlight", { color : "red" }, 1000);
					} else 
						$("#preview").effect("none", {}, 0);
				}
			}
		}
	}
}


/**
* No script, utilizamos v�rias imagens que ser�o exibidas e escondidas enquanto
* estiver rodando, por isso � importante pre-carregar as imagens que ser�o usadas
* para n�o deixar o script com comportamento estranho.
*
* Esta fun��o foi copiada de http://www.mattfarina.com/2007/02/01/preloading_images_with_jquery
* @param lista de strings contendo o caminho das imagens a serem carregadas previamente
* @return none
*/
jQuery.preloadImages = function()
{
  for(var i = 0; i<arguments.length; i++)
  {
    jQuery("<img>").attr("src", arguments[i]);
  }
}

/**
* ZMC exibe somente algumas linhas de di�logo por vez, todas elas sequenciais,
* por isso precisamos que esta fun��o que 'descobre' a parte correta do texto
* que deve ser exibida.
*
* Dado um offset no texto, descobre os limites do bloco atual de dados e retorna
* as duas linhas relacionadas ao di�logo que est� sendo exibido.
*
* @param txt o texto base para os c�lculos
* @param offset � a posi��o no texto em que o usu�rio se encontra
* @return um array com duas strings, contendo as duas linhas de di�logo que deveriam ser exibidas
*/
function getBlock(txt, offset) {
	var start = -1;
	var end = -1;
	var pos = offset;
	var block = '';
	var line = -1;
	var part = '';

	if(txt) {
		//encontra o inicio do bloco
		while((pos > 0) && (txt.charAt(pos) != "*")) {
			pos--;
			if(txt.charAt(pos) == "\n") line++;
		}
		
		start = pos;
		if(start != 0) start += 2;
		
		//encontra o fim do bloco
		pos = offset;
		while((pos < txt.length) && (txt.charAt(pos) != "*")) 
			pos++;
			
		end = pos - 1;
		
		// Se n�o tiver ocorrido nenhum erro
		if(line != -1) {
			//Separa todas as linhas em um array
			block = txt.substring(start, end + 1).split("\n");
			
			var p = line - (line % LINE_NUMBER);
			for(var n = 0; (n < LINE_NUMBER) && (p < block.length); n++) {
				part += block[p++] + (n != (LINE_NUMBER - 1) ? "\n" : "" );
			}
			/*if(line % 2) {	//caso a linha atual seja par, l� tamb�m a linha anterior
				part += (block[line-1] ? block[line-1] : '') + "\n";
				part += block[line];
			} else {		//caso seja �mpar, l� tamb�m a pr�xima linha
				part += block[line] + "\n";
				part += block[line+1] ? block[line+1] : '';
			}*/
		}
	}
	return part;
}