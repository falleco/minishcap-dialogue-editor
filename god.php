<?php
// Primeiro conferimos se a pessoa tem autoriza��o para acessar essa p�gina
if (!defined('DOKU_COOKIE')) define('DOKU_COOKIE', 'DW'.md5('/projects/zmc/'));
require_once("../inc/init.php");
require_once("../inc/auth.php");
$log = auth_login("", "", false, true);

if( isset($_REQUEST["type"]) && isset($_REQUEST["script"]) ) {

	/**
	* Simplesmente l� os dados dos scripts
	*/
	if($_REQUEST["type"] == "get") {
		$fileName = "./scripts/" . $_REQUEST["script"];
		$fh = fopen($fileName, 'r');

		$theData = fread($fh, filesize($fileName));
		fclose($fh);
		echo utf8_encode($theData);
	/**
	* Recebe e salva as altera��es nos scripts armazenados no servidor
	*/
	} elseif(($_REQUEST["type"] == "save") && isset($_REQUEST["data"])) {
		if(!$log)
			die("ERRO: Voc� n�o tem permiss�o para acessar esta p�gina.");
		
		//
		$fileName = "./scripts/" . $_REQUEST["script"];
		$fh = fopen($fileName, 'w');
		fwrite($fh, utf8_decode($_REQUEST["data"]));
		fclose($fh);
		
		$fh = fopen("./AccessLog.txt", 'a+');
		fwrite($fh, date("F j, Y, g:i a").":".$_SERVER['REMOTE_USER']." - ".$_REQUEST["script"]."\n");		
		fclose($fh);
		
		//loga a altera��o
		
		// D� um retorno � p�gina para a gente saber que ocorreu tudo bem
		echo "SALVO";
	/**
	* Algum erro no pedido
	*/
	} else {
		echo $_REQUEST["type"];
	}

/**
* Erro ao informar o tipo
*/
} else
echo "aff";
?>